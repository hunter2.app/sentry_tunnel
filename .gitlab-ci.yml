---
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

variables:
  GIT_SUBMODULE_STRATEGY: recursive

.base_job:
  needs:
    - job: version
      artifacts: true
  before_script:
    - set -x
    - export IMAGE_VERSION="$(cat version)"

.image_job:
  extends: .base_job
  image:
    name: gcr.io/kaniko-project/executor:v1.23.2-debug
    entrypoint: [""]
  stage: build
  script:
    - /kaniko/executor
      --cache
      --cache-copy-layers
      --context "${CI_PROJECT_DIR}/sentry_tunnel"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${IMAGE_VERSION}-${ARCH}"
    - echo "linux/${ARCH}" > "${ARCH}.platform"
  artifacts:
    paths:
      - "*.platform"

version:
  image:
    name: alpine/git:2.45.2
    entrypoint: [""]
  stage: build
  needs: []
  script:
    - if [[ -n "${CI_COMMIT_BRANCH}" && "${CI_COMMIT_BRANCH}" != "${CI_DEFAULT_BRANCH}" ]]; then echo -n "${CI_COMMIT_REF_SLUG}_" >> version; fi
    - echo -n "$(git -C sentry_tunnel describe --tags)-$(git describe --always)" >> version
  artifacts:
    paths:
      - version

image_amd64:
  extends: .image_job
  tags:
    - saas-linux-medium-amd64
  variables:
    ARCH: amd64

image_arm64:
  extends: .image_job
  rules:
    - if: $CI_PROJECT_NAMESPACE == "hunter2.app"
  tags:
    - saas-linux-medium-arm64
  variables:
    ARCH: arm64

manifest:
  extends: .base_job
  image: alpine:3.20
  stage: build
  needs:
    - !reference [.base_job, needs]
    - image_amd64
    - job: image_arm64
      optional: true
  variables:
    MANIFEST_TOOL_VERSION: 2.1.7
  script:
    - wget "https://github.com/estesp/manifest-tool/releases/download/v${MANIFEST_TOOL_VERSION}/binaries-manifest-tool-${MANIFEST_TOOL_VERSION}.tar.gz"
    - tar -x -f "binaries-manifest-tool-${MANIFEST_TOOL_VERSION}.tar.gz" manifest-tool-linux-amd64
    - platforms=$(cat *.platform | tr '\n' ',')
    - ./manifest-tool-linux-amd64
      --username "${CI_REGISTRY_USER}"
      --password "${CI_REGISTRY_PASSWORD}"
      push from-args
      --platforms "${platforms%,}"
      --template "${CI_REGISTRY_IMAGE}:${IMAGE_VERSION}-ARCH"
      --target "${CI_REGISTRY_IMAGE}:${IMAGE_VERSION}"

release:
  extends: .base_job
  stage: deploy
  needs:
    - manifest
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  image:
    name: gcr.io/go-containerregistry/crane/debug:v0.15.2
    entrypoint: [""]
  script:
    - crane auth login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - crane tag "${CI_REGISTRY_IMAGE}:${IMAGE_VERSION}" latest
